# Otto

Create, collect and review assignments for your classes

## Requirements

- [Yarn](https://yarnpkg.com/en/)
- [Parcel](https://parceljs.org/)

## Installation

```sh
yarn install
```

## License

Otto is distributed under the terms of the Apache License (Version 2.0).

See [LICENSE](./LICENSE) for details.